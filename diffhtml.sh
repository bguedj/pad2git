#!/bin/bash

# This can be used in order to have a meaningful diff in "git log -p":
#
# Use as: 
#
# GIT_EXTERNAL_DIFF=./diffhtml.sh git log -p --ext-diff pad.html
#

if ! [[ $2 =~ \.html$ ]] || ! [[ $5 =~ \.html$ ]] ; then
    exec diff "$2" "$5"
fi

t=$(mktemp -d /tmp/XXXXXXXXX)
trap "rm -rf $t" EXIT

xmllint -html -xmlout "$2" > "$t/$(basename $2 .html).xml"
xmllint -html -xmlout "$5" > "$t/$(basename $5 .html).xml"
xmldiff "$t/$(basename $2 .html).xml" "$t/$(basename $5 .html).xml"
