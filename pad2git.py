#!/usr/bin/env python3

import os
import time
import getpass
import getopt
import sys
import logging
import hashlib
import requests
import json
import urllib.parse
import re

try:
    from selenium import webdriver
    from selenium.webdriver.chrome.options import Options
    from selenium.webdriver.support.ui import WebDriverWait
    from selenium.webdriver.support import expected_conditions as EC
    from selenium.webdriver.common.by import By
    selenium_import_error = None
except ImportError as e:
    selenium_import_error = e

class ANSI(object):
    """ Class defining some ANSI control sequences, for example for
    changing text color """
    CSI = '\x1b[' # ANSI Control Sequence Introducer. Not the TV show
    SGR = 'm' # Set Graphics Rendition code
    NORMAL = CSI + '0' + SGR
    BLACK = CSI + '30' + SGR
    GREY = CSI + '30;1'
    RED = CSI + '31' + SGR
    BRIGHTRED = CSI + '31;1' + SGR
    GREEN = CSI + '32' + SGR
    BRIGHTGREEN = CSI + '32;1' + SGR
    YELLOW = CSI + '33' + SGR
    BRIGHTYELLOW = CSI + '33;1' + SGR
    BLUE = CSI + '34' + SGR
    BRIGHTBLUE = CSI + '34;1' + SGR
    MAGENTA = CSI + '35' + SGR
    BRIGHTMAGENTA = CSI + '35;1' + SGR
    CYAN = CSI + '36' + SGR
    BRIGHTCYAN = CSI + '36;1' + SGR
    # There is another grey with code "37" - white without intensity
    # Not sure if it is any different from "30;1" aka "bright black"
    WHITE = CSI + '37;1' + SGR

class ScreenFormatter(logging.Formatter):
    """ Class for formatting logger records for screen output, optionally
    with colorized logger level name (like cadofct.pl used to). """
    colors = {
        logging.INFO : ANSI.BRIGHTGREEN,
        logging.WARNING : ANSI.BRIGHTYELLOW,
        logging.ERROR : ANSI.BRIGHTRED,
        logging.FATAL : ANSI.BRIGHTRED,
    }

    # Format string that switches to a different color (with ANSI code
    # specified in the 'color' key of the log record) for the log level name,
    # then back to default text rendition (ANSI code in 'nocolor')
    colorformatstr = \
        '%(padding)s%(color)s%(levelnametitle)s%(nocolor)s:%(name)s: %(message)s'
    # Format string that does not use color changes
    nocolorformatstr = \
        '%(padding)s%(levelnametitle)s:%(name)s: %(message)s'

    def __init__(self, color = True):
        self.usecolor = color
        if color:
            super().__init__(fmt=self.colorformatstr)
        else:
            super().__init__(fmt=self.nocolorformatstr)

    def format(self, record):
        # Add attributes to record that our format string expects
        if self.usecolor:
            record.color = self.colors.get(record.levelno, ANSI.NORMAL)
            record.nocolor = ANSI.NORMAL
        record.levelnametitle = record.levelname.title()
        if hasattr(record, "indent"):
            record.padding = " " * record.indent
        else:
            record.padding = ""
        return super().format(record)

class gitlab_connection(object):
    def __init__(self, url, token, debug_api_calls=False):
        # simple provision if the "/api/v4" part is missing.
        url = re.sub("/$", "", url)
        if re.match("^https?://[^/]*$", url):
            url += "/api/v4"
        self.url = url
        self.token = token
        self.debug_api_calls = debug_api_calls

    def __pc(self, text):
        if not self.debug_api_calls:
            return
        print(ANSI.MAGENTA + text + ANSI.NORMAL)

    def nonapi_url(self):
        return re.sub("/api/v\d+$", "", self.url)

    def check_return_headers_only(self, t, endpoint, req, expected_return):
        e = expected_return
        if e is not None and req.status_code not in e:
            msg = "gitlab {} {} returned unexpected code {}".format(
                    t, endpoint, req.status_code)
            msg += "; is your token correct ?"
            msg += "\n" + str(req.headers) + "\n"
            msg += "\n" + str(req.content) + "\n"
            raise RuntimeError(msg)
        return req

    def check_return(self, t, endpoint, req, expected_return):
        e = expected_return
        msg = ""
        has_error = e is not None and req.status_code not in e
        if has_error:
            msg = "gitlab {} {} returned unexpected code {}".format(
                    t, endpoint, req.status_code)
            msg += "; is your token correct ?"
        json_parsed = None
        try:
            # at least DELETE calls sometimes return an empty string
            if not re.match(b"^\s*$", req.content):
                json_parsed = json.loads(req.content)
        except json.decoder.JSONDecodeError as ex:
            if not has_error:
                msg += "gitlab {} {} returned malformed json:\n".format(
                        t, endpoint)
            else:
                msg += "\n" + "Malformed json returned:\n"
            msg += req.content.decode("UTF-8")
            has_error = True
        if has_error:
            if json_parsed is not None:
                msg += "\n" + json.dumps(json.loads(req.content), indent=2)
            raise RuntimeError(msg)
        return json_parsed

    def get(self, endpoint, expected_return=None, **kwargs):
        headers={}
        if self.token:
            headers["PRIVATE-TOKEN"]=self.token
        self.__pc(">>> GET {} [{}]".format(self.url + endpoint, headers))
        req = requests.get(self.url + endpoint, headers=headers)
        return self.check_return("GET", endpoint, req, expected_return)

    def post(self, endpoint, expected_return=None, files=None, **kwargs):
        headers={}
        if self.token:
            headers["PRIVATE-TOKEN"]=self.token
        self.__pc(">>> POST {} [{}] [{}]".format(self.url + endpoint, headers, kwargs))
        req = requests.post(self.url + endpoint,
                headers=headers,
                files=files,
                json=kwargs)
        return self.check_return("POST", endpoint, req, expected_return)

    def head(self, endpoint, expected_return=None):
        headers={}
        if self.token:
            headers["PRIVATE-TOKEN"]=self.token
        self.__pc(">>> HEAD {} [{}]".format(self.url + endpoint, headers))
        req = requests.head(self.url + endpoint, headers=headers)
        return self.check_return_headers_only("HEAD", endpoint, req, expected_return)

    def project(self, project, *args, **kwargs):
        return gitlab_project_connection(self, project, *args, **kwargs)


class gitlab_project_connection(object):
    def __init__(self, gl, project, verbose=False):
        self.gl = gl
        self.name=project

    def get(self, sub_endpoint, *args, **kwargs):
        """
        mangles the endpoint, and passes it to the gitlab object
        """
        p = urllib.parse.quote(self.name, safe='')
        e = "/projects/{}{}".format(p, sub_endpoint)
        return self.gl.get(e, *args, **kwargs)

    def post(self, sub_endpoint, *args, **kwargs):
        """
        mangles the endpoint, and passes it to the gitlab object
        """
        p = urllib.parse.quote(self.name, safe='')
        e = "/projects/{}{}".format(p, sub_endpoint)
        return self.gl.post(e, *args, **kwargs)

    def head(self, sub_endpoint, *args, **kwargs):
        """
        mangles the endpoint, and passes it to the gitlab object
        """
        p = urllib.parse.quote(self.name, safe='')
        e = "/projects/{}{}".format(p, sub_endpoint)
        return self.gl.head(e, *args, **kwargs)

class ScreenHandler(logging.StreamHandler):
    def __init__(self, lvl = logging.INFO, color = True, **kwargs):
        super().__init__(**kwargs)
        self.setLevel(lvl)
        self.setFormatter(ScreenFormatter(color = color))


class browser(webdriver.Chrome):
    def __init__(self, pad_address, headless=True, password=None):
        """
        just creates the browser object
        """
        chrome_options = Options()
        # --no-sandbox is needed to run this in a container.
        chrome_options.add_argument('--no-sandbox')
        if headless:
            chrome_options.add_argument("--headless")
        chrome_options.add_argument("--enable-javascript")
        self.password = password
        self.pad_address = pad_address
        # chrome_options.headless = True # also works
        super().__init__(options=chrome_options)
        self.logger = logging.getLogger("browser")
        self.logger.setLevel(logging.INFO)
        self.wait = WebDriverWait(self, 10)

    def check_permission_denied_message(self):
        pd = self.find_elements(By.ID, "permissionDenied")
        if pd and pd[0].is_displayed():
            self.logger.critical("Permission denied")
            raise RuntimeError("Permission denied")

    def check_wrong_password(self):
        pd = self.find_elements(By.ID, "wrongPassword")
        if pd and pd[0].is_displayed():
            self.logger.critical("Wrong password")
            raise RuntimeError("Wrong password")


    def load(self):
        """ 
        loads the pad, fills in the password if needed
        """
        self.logger.info("Loading pad...")
        self.get(self.pad_address)
        eb = (By.ID, 'editorcontainerbox')
        self.wait.until(EC.presence_of_element_located(eb))

        self.check_permission_denied_message()
        self.supply_password_is_needed()
        self.wait.until(EC.presence_of_element_located((By.NAME, 'ace_outer')))

    def supply_password_is_needed(self):
        pr = self.find_elements(By.ID, "passwordRequired")
        if not pr or not pr[0].is_displayed():
            return

        pr = pr[0]

        self.logger.info("This pad is protected by a password")
        if self.password is None:
            self.password = getpass.getpass("Pad password: ")
            if self.password is None:
                self.logger.critical("Empty password provided")
                raise RuntimeError("Empty password")
        else:
            self.logger.info("Using password supplied on the command line")

        xpath_submit = "//div[@id='passwordRequired']/form/input[@type='submit']"
        self.wait.until(EC.element_to_be_clickable((By.XPATH, xpath_submit)))
        p = pr.find_element(By.ID, "passwordinput")
        p.clear()
        p.send_keys(self.password)
        pr.find_element(By.XPATH, xpath_submit).click()

        eb = (By.ID, 'editorcontainerbox')
        self.wait.until(EC.presence_of_element_located(eb))
        self.check_permission_denied_message()
        self.check_wrong_password()

    def tell_my_name(self):
        """
        clicks on the user list button, and sign up as "pad2git robot"
        """
        userlist_button_xpath="//div[@id='editbar']/ul[@class='menu_right']/li[@data-key='showusers']//button"
        userlist=self.find_element(By.XPATH, userlist_button_xpath)

        e = self.find_element(By.ID, "myusernameedit")

        if not e.is_displayed():
            userlist.click()
            e.clear()
            e.send_keys("The pad2git robot")
            userlist.click()

    def focus(self):
        self.switch_to.frame(self.find_element(By.NAME, "ace_outer"))
        self.switch_to.frame(self.find_element(By.NAME, "ace_inner"))


def print_help():
    help_text="""
Usage: pad2git.py [options]

Recognized options:
    -p, --pad <pad url>     Connect to this pad
    -G  render graphical output while working
    -P, --password <password>  pad password
    -s, --delay <time>  delay between checks (defaults to 10 seconds)
    -t, --time <time>   duration in seconds (defaults to 30 seconds)
    --gitlabapi <gitlab api url>  gitlab api root
    --gitlabproject <gitlab project path>  gitlab project path with namespace, e.g. thome/pad2git
    --gitlabtoken <gitlab token>  gitlab access token
    --trigger   use this script as a helper to trigger a gitlab pipeline
"""
    print(help_text)


if __name__ == '__main__':
    try:
        opts, args = getopt.getopt(sys.argv[1:],
                "GT:p:P:s:t:d",
                [
                    "graphics",
                    "pad=",
                    "password=",
                    "delay=",
                    "time=",
                    "gitlabtoken=",
                    "gitlabproject=",
                    "gitlabapi=",
                    "trigger"
                    ])
    except getopt.GetoptError as e:
        print(e)
        print_help()
        sys.exit(1)

    delay = 10
    maxtime = 30
    pad = None
    pad_password = None
    headless = True
    debug = False
    trigger = False

    gitlab_api = os.environ.get('CI_API_V4_URL')
    gitlab_project = os.environ.get('CI_PROJECT_PATH')
    gitlab_token = os.environ.get('CI_JOB_TOKEN')

    for opt, arg in opts:
        if opt == '-h':
            print_help()
            sys.exit()
        elif opt in ["-G"]:
            headless = False
        elif opt in ["--trigger"]:
            trigger = True
        elif opt in ["-p", "--pad"]:
            pad = arg
        elif opt in ["-P", "--password"]:
            pad_password = arg
        elif opt in ["-d"]:
            debug = True
        elif opt in ["-s", "--delay"]:
            delay = int(arg)
        elif opt in ["-t", "--time"]:
            maxtime = int(arg)
        elif opt in ["--gitlabtoken", "-T"]:
            gitlab_token = arg
        elif opt in ["--gitlabapi"]:
            gitlab_api = arg
        elif opt in ["--gitlabproject"]:
            gitlab_project = arg

    lvl = logging.INFO
    if debug:
        lvl = logging.DEBUG
    logging.getLogger().addHandler(ScreenHandler(lvl = lvl))
    logging.getLogger().setLevel(lvl)

    if pad is None:
        logging.getLogger().critical("No pad address provided, please use -p")
        raise RuntimeError("No pad address")

    if gitlab_api is None:
        logging.getLogger().critical("No gitlab address provided, please use --gitlabapi")
        raise RuntimeError("No gitlab address")

    if gitlab_project is None:
        logging.getLogger().critical("No gitlab project provided, please use --gitlabproject")
        raise RuntimeError("No gitlab project")

    if gitlab_token is None:
        logging.getLogger().critical("No project access token address provided, please use -T")
        raise RuntimeError("No project access token")

    gl = gitlab_connection(gitlab_api, gitlab_token, debug_api_calls=debug)
    pr = gl.project(gitlab_project, verbose=True)

    if trigger:
        # just poke the gitlab api, try to trigger a pipeline.
        variables = [
                dict(key="PAD", value=pad),
                dict(key="PROJECT_ACCESS_TOKEN", value=gitlab_token),
                dict(key="INTERVAL", value=str(delay)),
                dict(key="MAXTIME", value=str(maxtime)),
                ]

        if pad_password is not None:
            variables.append(dict(key="PAD_PASSWORD", value=pad_password))

        ret = pr.post("/pipeline?ref=master", expected_return=[201], variables=variables)
        logging.getLogger().info(f"Pipeline created, see progress at " + ret["web_url"])
        sys.exit(0)

    if selenium_import_error is not None:
        raise e

    W = browser(pad, headless=headless, password=pad_password)

    req = pr.head("/repository/files/pad.html?ref=master", expected_return=[200,404])
    has_data = req.status_code == 200

    try:
        W.load()
        W.tell_my_name()
        W.focus()
        logging.getLogger().info(f"This script is set to run for a maximum time of {maxtime} seconds")

        t = 0
        hh = None
        while True:
            oldhash = hh
            data = W.page_source
            hh = hashlib.sha256(data.encode('utf-8')).hexdigest()
            logging.getLogger().info(f"{time.asctime()}: {len(data)} bytes, sha256={hh}, modifications:{hh != oldhash}")
            if hh != oldhash:
                if not has_data:
                    logging.getLogger().info(f"Creating pad.html in the git repository")
                    action = "create"
                else:
                    logging.getLogger().info(f"Updating pad.html in the git repository")
                    action = "update"


                commit = {
                        'branch': 'master',
                        'commit_message': 'automatic update by pad2git robot',
                        'actions': [
                            {
                                'action': action,
                                'file_path': 'pad.html',
                                'content': data
                            }
                        ]
                    }
                req = pr.post("/repository/commits", expected_return=[201], **commit)
                logging.getLogger().info(f"Update successful")
                has_data = True
            if t >= maxtime:
                break
            time.sleep(delay)
            t += delay

    except Exception as e:
        logging.getLogger().critical("Closing selenium driver")
        W.quit()
        raise e
